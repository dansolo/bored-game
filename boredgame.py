#!/usr/bin/python2

import pygame
import os, sys
import ConfigParser
import random ## LOl I'm so random lol :3 ~~~~ 

boardPath = 'boards/'
dicePath = 'dice/'
tokenPath = 'tokens/'
cfgFile = sys.argv[1]

class Token:
    def __init__(self, position, colour):
        self.position = position
        self.colour = colour
        if colour == 'red':
            self.image = tokenred
        elif colour == 'blue':
            self.image = tokenblue 
        elif colour == 'green':
            self.image = tokengreen 
        else:
            self.image = tokenpurple

    def getcollision(self, point):
        self.size = self.image.get_size()
        collisionArea = []
        for x in range(self.size[0]):
            for y in range(self.size[1]):
                collisionArea.append((self.position[0] + x, self.position[1] + y))
        if point in collisionArea:
            return True

    def move(self, position):
        self.position = (position[0]-self.image.get_size()[0]/2, position[1]-self.image.get_size()[1]/2)

    def draw(self, screen):
        screen.blit(self.image, self.position)

class Dice:
    def __init__(self, position):
        self.position = position

        self.faces = [
        pygame.image.load(os.path.join(dicePath, 'dice1.png')).convert_alpha(),
        pygame.image.load(os.path.join(dicePath, 'dice2.png')).convert_alpha(),
        pygame.image.load(os.path.join(dicePath, 'dice3.png')).convert_alpha(),
        pygame.image.load(os.path.join(dicePath, 'dice4.png')).convert_alpha(),
        pygame.image.load(os.path.join(dicePath, 'dice5.png')).convert_alpha(),
        pygame.image.load(os.path.join(dicePath, 'dice6.png')).convert_alpha()
        ]
       
        self.image = self.faces[0]
    def getcollision(self, point):
        self.size = self.image.get_size()
        collisionArea = []
        for x in range(self.size[0]):
            for y in range(self.size[1]):
                collisionArea.append((self.position[0] + x, self.position[1] + y))
        if point in collisionArea:
            return True
    
    def move(self, position):
        self.position = (position[0]-self.image.get_size()[0]/2, position[1]-self.image.get_size()[1]/2)

    def draw(self, screen):
        screen.blit(self.image, self.position)
    
    def roll(self):
        self.image = random.choice(self.faces)

config = ConfigParser.ConfigParser()
with open(os.path.join(cfgFile), "r") as c:
    config.readfp(c)
    noOfDice = int(config.get('Dice', 'numberofdice'))
    noOfRTokens = int(config.get('Tokens', 'numberofredtokens'))
    noOfGTokens = int(config.get('Tokens', 'numberofgreentokens'))
    noOfBTokens = int(config.get('Tokens', 'numberofbluetokens'))
    noOfPTokens = int(config.get('Tokens', 'numberofpurpletokens'))
    boardName = str(config.get('Board', 'board'))
    board = pygame.image.load(os.path.join(boardPath, boardName))
    title = config.get('Title', 'title')

screen = pygame.display.set_mode((1024, 600), pygame.FULLSCREEN)

pygame.display.set_caption(title)

tokenred = pygame.image.load(os.path.join(tokenPath, 'tokenred.png')).convert_alpha()
tokenblue = pygame.image.load(os.path.join(tokenPath, 'tokenblue.png')).convert_alpha()
tokengreen = pygame.image.load(os.path.join(tokenPath, 'tokengreen.png')).convert_alpha()
tokenpurple = pygame.image.load(os.path.join(tokenPath, 'tokenpurple.png')).convert_alpha()

board = board.convert_alpha()
board = pygame.transform.scale(board, screen.get_size())

screen.blit(board, (0, 0))

pygame.display.flip()
#######################################
## This part could be nicer.        ###
#######################################
tokens = []
for i in range(noOfBTokens):
    bluetoken = Token((50, 50), 'blue')
    tokens.append(bluetoken)


for i in range(noOfRTokens):
    redtoken = Token((200, 200), 'red')
    tokens.append(redtoken)

for i in range(noOfPTokens):
    purpletoken = Token((100, 100), 'purple')
    tokens.append(purpletoken)

for i in range(noOfGTokens):
    greentoken = Token((150, 150), 'green')
    tokens.append(greentoken)

dice = []

for i in range(noOfDice):
    die = Dice((500, 500))
    dice.append(die)
######################################

clickingdice = False
clickingtoken = False

while True:
    screen.fill((0, 0, 0))

    screen.blit(board, (0, 0))

    if pygame.key.get_pressed()[pygame.K_ESCAPE]:
        raise SystemExit

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            raise SystemExit

    if pygame.mouse.get_pressed()[0]:
        if not clickingdice and not clickingtoken:
            for t in tokens:
                if t.getcollision(pygame.mouse.get_pos()):
                    clickingtoken = True
                    heldtoken = t
                    break
            for d in dice:
                if d.getcollision(pygame.mouse.get_pos()):
                    clickingdice = True
                    helddice = d
                    break

    if clickingtoken:
        heldtoken.move(pygame.mouse.get_pos())
        if not pygame.mouse.get_pressed()[0]:
            clickingtoken = False
    if clickingdice:
        helddice.move(pygame.mouse.get_pos())
        helddice.roll()
        if not pygame.mouse.get_pressed()[0]:
            clickingdice = False
            helddice.roll()

    if tokens:
        for t in tokens:
            t.draw(screen)

    if dice:
        for d in dice:
            d.draw(screen)

    pygame.display.flip()

    pygame.event.pump()
    
